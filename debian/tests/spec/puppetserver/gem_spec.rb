require 'spec_helper'

describe "test puppetserver gem subcommand and jruby gem environment" do
  describe command('puppetserver gem env') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should contain('INSTALLATION DIRECTORY: /var/lib/puppetserver/jruby-gems') }
    its(:stdout) { should match(%r{GEM PATHS:\s+- /var/lib/puppetserver/jruby-gems\s+- /usr/lib/puppetserver/vendored-jruby-gems}m) }
  end

  describe command('puppetserver gem list') do
    its(:exit_status) { should eq 0 }
    # jruby stdlib gem
    its(:stdout) { should match(/^ffi \(default: [0-9\.]+ java\)/) }
    # vendored gem
    its(:stdout) { should match(/^concurrent-ruby \([0-9\.]+\)/) }
  end
end
