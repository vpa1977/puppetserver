require 'spec_helper'

describe "service is not enabled/started on install" do
  describe service('puppetserver') do
    it { should_not be_enabled }
    it { should_not be_running }
  end

  describe port(8140) do
    it { should_not be_listening }
  end
end

describe "service starts successfully" do
  describe command('puppet resource service puppetserver ensure=running') do
    its(:exit_status) { should eq 0 }
  end

  describe service('puppetserver') do
    it { should be_running }
  end

  describe port(8140) do
    it { should be_listening }
  end
end

describe "service https endpoint is responsive" do
  describe command('curl --silent --cacert "/etc/puppet/puppetserver/ca/ca_crt.pem" "https://$(hostname):8140/status/v1/simple"') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should eq 'running' }
  end
end

describe "access log records queries" do
  describe file('/var/log/puppetserver/puppetserver-access.log') do
    its(:content) { should contain '"GET /status/v1/simple HTTP/1.1" 200' }
  end
end

describe "call-home analytics and update checks disabled by default" do
  describe file('/var/log/puppetserver/puppetserver.log') do
    its(:content) { should match %r{Not submitting module metrics via Dropsonde -- submission is disabled} }
    its(:content) { should match %r{Not checking for updates - opt-out setting exists} }
  end
end
