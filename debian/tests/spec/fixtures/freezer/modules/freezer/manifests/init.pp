class freezer {
  file {
    '/tmp/freezer':
      ensure  => directory,
      recurse => true,
      source  => 'puppet:///modules/freezer',
    ;
  }
}
