Puppet Server in Debian
========================

1. Differences from upstream Puppet Server
------------------------------------------

 - To comply with the Debian Policy Manual, Puppet Server's default paths are
   different from those shipped by upstream (Puppetlabs). Rather than paths
   under /opt/puppetlabs, Puppet Server in Debian uses the following:

   * cadir: /etc/puppet/puppetserver/ca
   * config: /etc/puppet/puppetserver/conf.d
   * logdir: /var/log/puppetserver
   * vardir: /var/lib/puppetserver

   In addition, the base Puppet library in Debian (puppet-agent) uses these
   paths which are also relevant for Puppet Server:

   * confdir: /etc/puppet
   * codedir: /etc/puppet/code
   * moduledir: /usr/share/puppet/modules

 - Automatic checks for newer upstream versions are disabled by default, as
   they are using a call-home HTTP request to determine update availability.
   If you wish to re-enable them, create a 'product.conf' file in the config
   directory and insert the snippet below:

       # Enabling automatic upstream version checks
       product: {
           check-for-updates: true
       }

  - Upstream ships its version of Puppet Server with enabled-by-default
    telemetry submission. This functionality relies on Dropsonde, a Ruby gem
    which is not available in Debian. Thus, in Debian, telemetry submission is
    unsupported and disabled out-of-the-box.

  - In Debian, Puppet Server uses the system-provided JRuby interpreter, while
    upstream bundles a copy of its own.

2. Logging
----------

  Logging for Puppet Server is configured using these two configuration files:

  /etc/puppet/puppetserver/logback.xml
  /etc/puppet/puppetserver/request-logging.xml

  These may be modified to adjust logging levels, retention period, rotation, or
  disable logging in part or entirely.

3. JRuby
--------

  While Puppet Server relies on the system JRuby interpreter, it uses a
  runtime environment independent of the system's. This environment may be
  examined using the "puppetserver gem env" command.

  To display a list of Ruby gems known to Puppet Server's JRuby, execute the
  "puppetserver gem list" command.

  To install additonal gems into this environment, the "gem install" subcommand
  may be used, for example: "puppetserver gem install trocla".
