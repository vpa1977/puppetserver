================
puppetserver-irb
================

-----------------------------
Puppetserver interactive ruby
-----------------------------

:Author: Louis-Philippe Véronneau
:Date: 2023
:Manual section: 1

Synopsis
========

| ``puppetserver`` `irb` (--help)
| ``puppetserver`` `irb` <options>

Description
===========

Start an interactive REPL for the JRuby that Puppet Server uses.

More information
================

This command behaves exactly like the regular Ruby ``irb`` command, but uses
Puppet Server's JRuby environment.

For more information on how to use this command, see the ``irb``\ (1) man page.

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://tickets.puppetlabs.com/browse/SERVER

See Also
========

``puppetserver``\ (1), ``puppetserver-foreground``\ (1), ``puppetserver-ruby``\
(1), ``puppetserver-gem``\ (1), ``puppetserver-ca``\ (1),
