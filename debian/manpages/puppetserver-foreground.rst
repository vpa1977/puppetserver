=======================
puppetserver-foreground
=======================

---------------------------------------
Puppetserver foreground process command
---------------------------------------

:Author: Louis-Philippe Véronneau
:Date: 2023
:Manual section: 1

Synopsis
========

| ``puppetserver`` `foreground` (--help)
| ``puppetserver`` `foreground` <options>

Description
===========

Start the Puppet Server as a foreground process.

Options
=======

-h, --help
   Show the help message and exit

-d, --debug
   Turn on debug mode.

-b, --bootstrap-config `BOOTSTRAP-CONFIG-FILE`
   Path to bootstrap config file

-c, --config `CONFIG-PATH`
   Path to a configuration file or directory of configuration files, or a
   comma-separated list of such paths. See the documentation for a list of
   supported file types.

-p, --plugins `PLUGINS-DIRECTORY`
   Path to directory plugin .jars

-r, --restart-file `RESTART-FILE`
   Path to a file whose contents are incremented each time all of the
   configured services have been started.

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://tickets.puppetlabs.com/browse/SERVER

See Also
========

``puppetserver``\ (1), ``puppetserver-gem``\ (1), ``puppetserver-ruby``\
(1), ``puppetserver-irb``\ (1), ``puppetserver-ca``\ (1),
