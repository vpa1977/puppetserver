=================
puppetserver-ruby
=================

-----------------
Puppetserver ruby
-----------------

:Author: Louis-Philippe Véronneau
:Date: 2023
:Manual section: 1

Synopsis
========

| ``puppetserver`` `ruby` (--help)
| ``puppetserver`` `ruby` <options>

Description
===========

Run code in Puppet Server's JRuby interpreter.

More information
================

This command behaves exactly like the regular JRuby ``ruby`` command, but uses
Puppet Server's JRuby environment.

For more information on how to use this command, see the ``jruby``\ (1) man page.

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://tickets.puppetlabs.com/browse/SERVER

See Also
========

``puppetserver``\ (1), ``puppetserver-foreground``\ (1), ``puppetserver-irb``\
(1), ``puppetserver-gem``\ (1), ``puppetserver-ca``\ (1),
