================
puppetserver-gem
================

---------------------------
Puppetserver gem management
---------------------------

:Author: Louis-Philippe Véronneau
:Date: 2023
:Manual section: 1

Synopsis
========

| ``puppetserver`` `gem` (--help)
| ``puppetserver`` `gem` <options>

Description
===========

Install and manage gems that are isolated from system Ruby and are accessible
only to Puppet Server.

More information
================

This command behaves exactly like the regular Ruby ``gem`` command, but
installs and manages isolated gems that are only accessible to Puppet Server.

For more information on how to use this command, see the ``gem``\ (1) man page.

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://tickets.puppetlabs.com/browse/SERVER

See Also
========

``puppetserver``\ (1), ``puppetserver-foreground``\ (1), ``puppetserver-ruby``\
(1), ``puppetserver-irb``\ (1), ``puppetserver-ca``\ (1),
