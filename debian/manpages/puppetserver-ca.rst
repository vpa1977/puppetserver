===============
puppetserver-ca
===============

----------------------------------
Puppetserver CA management command
----------------------------------

:Author: Louis-Philippe Véronneau
:Date: 2024
:Manual section: 1

Synopsis
========

| ``puppetserver`` `ca` (--help | --version)
| ``puppetserver`` `ca` (--verbose) [`subcommand`] <args>

Description
===========

Manage the Private Key Infrastructure for Puppet Server's built-in Certificate
Authority.

Options
=======

-h, --help
   Show the help message and exit

--version
   Show the version number of the CA utility and exit

--verbose
   Display low-level information

Subcommands
===========

Certificate Actions
-------------------

The following subcommands require a running Puppet Server:

``clean`` `<args>` ...
    Revoke cert(s) and remove related files from CA

``generate`` `<args>` ...
    Generate a new certificate signed by the CA

``list`` `<args>` ...
    List certificates and CSRs

``revoke`` `<args>` ...
    Revoke certificate(s)

``sign`` `<args>` ...
    Sign certificate request(s)


Administration Actions
----------------------

The following subcommands require Puppet Server to be stopped:

``delete`` `<args>` ...
    Delete signed certificate(s) from disk

``import`` `<args>` ...
    Import an external CA chain and generate server PKI

``setup`` `<args>` ...
    Setup a self-signed CA chain for Puppet Server

``enable`` `<args>` ...
    Setup infrastructure CRL based on a node inventory

``migrate`` `<args>` ...
    Migrate the existing CA directory to /etc/puppetserver/ca

``prune`` `<args>` ...
    Prune the local CRL on disk to remove any duplicated certificates

For more details on the arguments supported by these subcommands, see the
"Arguments" section of this man page.

Arguments
=========

| **clean**:
|    --certname `NAME[,NAME]`           One or more comma separated certnames
|    --config `CONF`                    Custom path to puppet.conf

| **delete**:
|    --config `CONF`                    Path to puppet.conf
|    --expired                        Delete expired signed certificates
|    --revoked                        Delete signed certificates that have already been revoked

| **enable**:
|    --config `CONF`                    Path to puppet.conf
|    --infracrl                       Create auxiliary files for the infrastructure-only CRL

| **generate**:
|    --certname `NAME[,NAME]`           One or more comma separated certnames
|    --config `CONF`                    Path to puppet.conf
|    --subject-alt-names `NAME[,NAME]`  One or more comma separated alt-names for the cert
|    --ca-client                      Whether this cert will be used to request CA actions
|    --force                          Suppress errors when signing cert offline
|    --ttl `TTL`                        The time-to-live for each cert generated and signed

| **import**:
|    --config `CONF`                    Path to puppet.conf
|    --private-key `KEY`                Path to PEM encoded key
|    --cert-bundle `BUNDLE`             Path to PEM encoded bundle
|    --crl-chain `CHAIN`                Path to PEM encoded chain
|    --certname `NAME`                  Common name to use for the server cert
|    --subject-alt-names `NAME[,NAME]`  One or more comma separated alt-names for the cert

| **list**:
|    --config `CONF`                    Custom path to Puppet's config file
|    --all                            List all certificates
|    --format `FORMAT`                  Valid formats are: 'text' (default), 'json'
|    --certname `NAME[,NAME]`           List the specified cert(s)

| **migrate**:
|    --config `CONF`                    Path to puppet.conf

| **prune**:
|    --config `CONF`                    Path to the puppet.conf file on disk
|    --remove-duplicates              Remove duplicate entries from CRL(default)
|    --remove-expired                 Remove expired  entries from CRL
|    --remove-entries                 Remove entries from CRL
|    --serial `NUMBER[,NUMBER]`         Serial numbers(s) in HEX to be removed from CRL
|    --certname `NAME[,NAME]`           Name(s) of the cert(s) to be removed from CRL

| **revoke**:
|    --certname `NAME[,NAME]`           One or more comma separated certnames
|    --config `CONF`                    Custom path to puppet.conf

| **setup**:
|    --config `CONF`                    Path to puppet.conf
|    --subject-alt-names `NAME[,NAME]`  One or more comma separated alt-names for the cert
|    --ca-name `NAME`                   Common name to use for the CA signing cert
|    --certname `NAME`                  Common name to use for the server cert

| **sign**:
|    --ttl `TTL`                        The time-to-live for each cert signed
|    --certname `NAME[,NAME]`           The name(s) of the cert(s) to be signed
|    --config `CONF`                    Custom path to Puppet's config file
|    --all                            Operate on all certnames

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://tickets.puppetlabs.com/browse/SERVER

See Also
========

``puppetserver``\ (1), ``puppetserver-gem``\ (1), ``puppetserver-ruby``\
(1), ``puppetserver-irb``\ (1), ``puppetserver-foreground``\ (1),
