============
puppetserver
============

-------------------------
Puppetserver main command
-------------------------

:Author: Louis-Philippe Véronneau
:Date: 2023
:Manual section: 1

Synopsis
========

| ``puppetserver (--help | --version)``
| ``puppetserver`` [`command`] <args>

Description
===========

``puppetserver`` implements Puppet's server-side components for managing Puppet
agents in a distributed, service-oriented architecture.

Options
=======

-h, --help
   Show the help message and exit. At top level it only lists the commands. To
   display the help of a specific subcommand, add the ``--help`` flag *after*
   the said command name

-v, --version
   Show the version number and exit

Commands
========

``puppetserver`` has eight different commands:

``ca`` `<args>` ...
    Manage the Private Key Infrastructure for Puppet Server's built-in
    Certificate Authority

``gem`` `<args>` ...
    Install and manage gems that are isolated from system Ruby and are
    accessible only to Puppet Server.

``ruby`` `<args>` ...
    Run code in Puppet Server's JRuby interpreter.

``irb`` `<args>` ...
    Start an interactive REPL for the JRuby that Puppet Server uses.

``foreground`` `<args>` ...
    Start the Puppet Server as a foreground process.

``start`` `<args>` ...
    Start the Puppet Server as a background process.

``stop`` `<args>` ...
    Stop the Puppet Server process.

``reload`` `<args>` ...
    Reload the Puppet Server process.

For more details on the commands, see their respective manual pages.

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://tickets.puppetlabs.com/browse/SERVER

See Also
========

``puppetserver-ca``\ (1), ``puppetserver-gem``\ (1), ``puppetserver-ruby``\
(1), ``puppetserver-irb``\ (1), ``puppetserver-foreground``\ (1),
